# Trabajo práctico 2 - Introducción a la inteligencia artificial
Integrantes: Delfina Martín, Sebastián Morales

## Heurística utilizada para el problema de las 4 esquinas

> Vale la pena recordar que el estado que elegimos para estre problema consiste en la posición actual del pacman y 4 valores booleanos que se corresponden con cada una de las esquinas. Estos valores indican si se recorrió la esquina en cuestión.

La heurística que implementamos para el problema de las 4 esquinas distingue 5 casos los cuales están asociados a la cantidad de esquinas que se recorrieron hasta el momento. A continuación desarrollaremos la idea detrás de cada uno de estos casos. Para más información ver la implementación en *searchAgent.py*.

- 4 esquinas por recorrer: recorremos la esquina con distancia de manhattan menor y luego visitamos las restantes en el orden que minimice la suma de las distancias a lo largo de los bordes del tablero.

Esto en la implementación se traduce a asignarle a cada nodo estado del problema el valor

> min(distM(pos_actual,e1), distM(pos_actual,e2), distM(pos_actual,e3), distM(pos_actual,e4)) + min(ancho + alto + ancho, alto + ancho + alto)

o lo que es lo mismo

> min(distM(pos_actual,e1), distM(pos_actual,e2), distM(pos_actual,e3), distM(pos_actual,e4)) + ancho + alto + min(ancho, alto)

Suponiendo que pos_actual es nuestra posición actual.

- 3 esquinas por recorrer: recorremos la esquina adyacente a la ya visitada con distancia de manhattan menor y luego visitamos las 2 esquinas restantes a lo largo de los bordes del tablero.

Esto en la implementación se traduce a asignarle a cada nodo estado del problema el valor

> min(distM(pos_actual,e1_ady), distM(pos_actual,e2_ady)) + alto + ancho

suponiendo que que pos_actual es nuestra posición actual y e1_ady y e2_ady son esquinas no visitadas adyacentes a la esquina visitada.

- 2 esquinas por recorrer: recorremos la esquina no visitada con distancia de manhattan menor y luego visitamos la restante.

Esto en la implementación se traduce a asignarle a cada nodo estado del problema el valor

> min(distM(pos_actual,e1), distM(pos_actual,e2)) + distM(e1,e2)

- 1 esquina por recorrer: recorremos la esquina restante.

Esto en la implementación se traduce a asignarle a cada nodo estado del problema el valor

> distM(pos_actual,e)

suponiendo que pos_actual es nuestra posición actual y e es la única esquina no visitada.

- 0 esquinas por recorrer: solución del problema. Esto en la implementación se traduce a asignarle a cada nodo estado del problema el valor 0 (mínimo valor de h).

### Admisibilidad

Como la heurística surge de un problema relajado (recorrer un laberinto sin paredes) resulta evidente que el cálculo de h para cada nodo del árbol de búsqueda o equivalentemente del grafo espacio de estados no sobreestima el costo real.

### Consistencia

Para analizar informalmente la consistencia del problema distinguimos dos casos:

1. El estado actual y un sucesor tienen la misma cantidad de esquinas visitadas. En particular tienen marcadas exactamente las mismas esquinas visitadas.
2. El estado actual y un sucesor tienen diferente cantidad de esquinas visitadas. En particular el sucesor tiene exactamente una esquina más visitada. Más aún, la posición del sucesor coincide con la posición de una esquina.

En el caso (1.) el valor de h para el estado actual y para el sucesor se computa a partir del mismo cálculo ya que la cantidad de esquinas a visitar es la misma para los dos nodos. Y como se vió en la primer sección, los cálculos de h en cada caso dependen únicamente de la distancia de manhattan entre la posición actual y las esquinas (el largo, el ancho y las distancias entre esquinas son constantes para un layout dado). Además, como el pacman avanza horiontalmente o verticalmente la diferencia de h entre un nodo y su sucesor se produce en un cálculo de distancia de manhattan y este puede ser a lo sumo 1 (y como mínimo 1, de otra forma la posición actual no se modificaría).

Si el pacman se acerca a la siguiente esquina a visitar:

> f(nodo_actual) = costo_ruta + h = (costo_ruta + 1) + (h - 1) = f(sucesor)

Si no:

> f(nodo_actual) = costo_ruta + h < (costo_ruta + 1) + (h + 1) = f(sucesor)

En el caso (2.) seguimos un análisis análogo al del caso (1.). Adicionalemente, sabemos que el cálculo que se realiza para un estado con n esquinas visitadas (h_{1}) es *estrictamente mayor* al que se realiza para un estado con n+1 esquinas visitadas (h_{2}) pero que esta diferencia es a lo sumo 1 (porque depende de distancias de manhattan). En símbolos: h_{1} + 1 = h_{2}

> f(nodo_actual) = costo_ruta + h_{1} = (costo_ruta + 1) + (h_{2} - 1) = f(sucesor)

Finalmente resulta que f(nodo_actual) <= f(sucesor) \forall nodo, es decir, h es consistente.
